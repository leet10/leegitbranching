/*
 * Generates and prints n random numbers between 1 and 100,
 * reading n in from a command line argument.
 * Created Feb 2022
 * Updated Feb 2022
 * @author Tou Ko Lee
 */

import java.util.Random;

/**
 *
 * @author Tou Ko Lee
 */
public class numbers {

    /**
     * Generates and prints n random numbers between 1 and 100,
     * @param n the number of numbers needed to be generated
     */
    public static void gen(int n){
        Random rand = new Random();
        for (int i = 0; i < n; i++){
            int randNum = rand.nextInt(100-1+1)+1;
            System.out.println(randNum);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // gets input from the user
        int n = Integer.parseInt(args[0]);
        
        // generates n numbers
        System.out.println("\nPrinting " + n + " random number(s) between 1 to 100:");
        gen(n);
    }
    
}
