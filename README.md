This application is used to create "n" numbers of random numbers from 1 to 100 (inclusive) from a user input.

To use the application, run numbers.java (java numbers n) after you compiled it(javac numbers.java) to create to create "n" number of random numbers from 1 to 100. 
Then it will print the random numbers.